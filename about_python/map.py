# Built-in Function MAP

class Divider:
    """Calculate if what number is multiple"""

    def __init__(self, numbers: list, divider):
        self._numbers = numbers
        self._divider = divider
        self._not_divider = -1

    def get(self) -> list:
        dividers = [self._divider] * len(self._numbers)
        result = map(self.__calculate, self._numbers, dividers)
        return list(result)

    def __calculate(self, number, divider):
        if (number % divider) == 0:
            return number
        else:
            return self._not_divider
