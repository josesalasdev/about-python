# Built-in Function MAP

class User:
    """Model User"""

    def __init__(self):
        self._users = [
            {'name': 'smitt'},
            {'name': 'simpson'}
        ]

    def find(self, name: str = None) -> list:
        self._name = name
        if not self._name:
            return self._users

        return self.__search()

    def __search(self) -> list:
        if {'name': self._name} in self._users:
            index = self._users.index({'name': self._name})
            return [self._users[index]]
        else:
            return []

    def __len__(self):
        return len(self._users)
