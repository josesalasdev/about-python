# Built-in Function MAX, MIN, SUM


class GetNumber:

    def calculate(self, numbers: list):
        """Calculate the min and max value.

        Args:
            numbers(list): List of numbers.
        """
        self._numbers = numbers
        result = map(
            self.__only_positives,
            self._numbers
            )
        print(f"I'm a {type(result)}")

        return {
            'max': max(self._numbers),
            'min': min(self._numbers),
            'sum': sum(result)
        }

    def __only_positives(self, number):
        """calculate positive numbers.

        Args:
            number: Number to calculate.
        """
        if number < 0:
            return 0
        else:
            return number
