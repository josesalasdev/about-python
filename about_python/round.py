# Built-in Function ROUND

class ApiRound:

    @staticmethod
    def calculate(a, b):
        if a == 0:
            raise f'{a} has to be greater than 0'
        result = a / b
        return result.__round__(4)
