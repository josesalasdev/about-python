# Built-in Function DIR

class User:

    def __init__(self):
        self.request = {}

    def __dir__(self):
        """Overwriting the method.

        Returns:
            list: returns list of arguments.
        """
        return list()
