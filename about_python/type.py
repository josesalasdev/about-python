# Built-in Function TYPE, metaprogramming 🤯


class String:

    def __new__(cls):
        return 'HelloWord'


class Name:

    def __init__(self, obj: str):
        self._obj = obj

    def valid(self) -> bool:
        if type(self._obj) == str:
            return True
        else:
            return False
