# Built-in Function EXEC, GLOBALS, LOCALS

g = 'Global'


def send():
    letter = f'hi, {g}'
    exec("print(letter)")
    return letter.upper()


def greeting():
    message = 'HelloWorld'
    print(locals())
    return message.upper()
